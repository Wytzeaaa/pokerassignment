﻿using System;
using UnityEngine;

namespace PokerAssignment {

    /// <summary>
    /// Represents a playing card.
    /// </summary>
    [Serializable]
    public struct Card {

        public RankType Rank => _rank;
        public SuitType Suit => _suit;

        [SerializeField] private RankType _rank;
        [SerializeField] private SuitType _suit;

        public Card (RankType rank, SuitType suit) {
            _rank = rank;
            _suit = suit;
        }

        public override string ToString () {
            return $"{base.ToString ()} ({Suit} {Rank})";
        }

        public enum RankType {
            R2,
            R3,
            R4,
            R5,
            R6,
            R7,
            R8,
            R9,
            R10,
            Jack,
            Queen,
            King,
            Ace
        }

        public enum SuitType {
            Diamond,
            Club,
            Heart,
            Spade
        }

    }
}
