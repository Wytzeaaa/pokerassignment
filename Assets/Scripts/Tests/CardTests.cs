﻿using NUnit.Framework;
using System.Collections;
using System.Linq;
using UnityEditor.Build.Content;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace PokerAssignment.Tests {

    public class CardTests {

        public const string MainSceneName = "SampleScene";

        [Test]
        public static void CardsWithSameRankAndSuitShouldBeEqual () {
            var card1 = new Card (Card.RankType.Ace, Card.SuitType.Heart);
            var card2 = new Card (Card.RankType.Ace, Card.SuitType.Heart);
            Assert.AreEqual (card1, card2);
        }

    }
}
