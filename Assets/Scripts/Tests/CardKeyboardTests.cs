﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Build.Content;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace PokerAssignment.Tests {

    public class CardKeyboardTests {

        public static Ui.CardKeyboard GetCardKeyboardInstance () {
            var uiController = UiControllerTests.GetUiControllerInstance ();
            var cardKeyboard = uiController.CardKeyboard;
            if (cardKeyboard == null) {
                Assert.Fail ("CardKeyboard instance could not be found.");
            }
            return cardKeyboard;
        }

        [Test]
        public void CardKeyboardShouldContainCardButtons () {
            var cardKeyboard = GetCardKeyboardInstance ();
            Assert.IsNotNull (cardKeyboard.CardButtons);
        }

        [Test]
        public void CardKeyboardShouldContain52CardButtons () {
            var cardKeyboard = GetCardKeyboardInstance ();
            Assert.AreEqual (52, cardKeyboard.CardButtons.Count);
        }

        [Test]
        public void AllCardButtonsShouldHaveCardProperty () {
            var cardKeyboard = GetCardKeyboardInstance ();
            for (int i = 0; i < cardKeyboard.CardButtons.Count; i++) {
                Assert.IsNotNull (cardKeyboard.CardButtons[i], $"Cardbutton with index {i} is null.");
            }
        }

        [Test]
        public void CardKeyboardShouldNotContainDuplicateCardButtons () {
            var cardKeyboard = GetCardKeyboardInstance ();
            for (int i = 0; i < cardKeyboard.CardButtons.Count; i++) {
                for (int j = 0; j < cardKeyboard.CardButtons.Count; j++) {
                    if (i != j) {
                        Assert.AreNotEqual (cardKeyboard.CardButtons[i].Card, cardKeyboard.CardButtons[j].Card, $"Cards on Cardbutton with index {i} and index {j} have the same Card property.");
                    }
                }
            }
        }

        [Test]
        public void CardKeyboardShouldContainBackspaceButton () {
            var cardKeyboard = GetCardKeyboardInstance ();
            Assert.IsNotNull (cardKeyboard.BackspaceButton);
        }

        [Test]
        public void CardKeyboardShouldContainEnterButton () {
            var cardKeyboard = GetCardKeyboardInstance ();
            Assert.IsNotNull (cardKeyboard.BackspaceButton);
        }

        [Test]
        public void CardKeyboardShouldSentEventWhenCardButtonClicked () {
            var cardKeyboard = GetCardKeyboardInstance ();
            List<Card> receivedEvents = new List<Card> ();
            cardKeyboard.CardClicked += delegate (Card card) {
                receivedEvents.Add (card);
            };
            for (int i = 0; i < cardKeyboard.CardButtons.Count; i++) {
                cardKeyboard.CardButtons[i].Clicked += (Card card) => {
                    receivedEvents.Add (card);
                };
                cardKeyboard.CardButtons[i].OnPointerClick (null);
            }
            Assert.AreEqual (cardKeyboard.CardButtons.Count, receivedEvents.Count);
        }
 
    }
}
