﻿using NUnit.Framework;
using System.Collections;
using System.Linq;
using UnityEditor.Build.Content;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace PokerAssignment.Tests {

    public class UiControllerTests {

        public static Ui.UiController GetUiControllerInstance () {
            var appController = AppControllerTests.GetAppControllerInstance ();
            var uiController = appController.UiController;
            if (uiController == null) {
                Assert.Fail ("UIController instance could not be found.");
            }
            return uiController;
        }

        [Test]
        public void CardKeyboardShouldBeAssigned () {
            var uiController = GetUiControllerInstance ();
            Assert.IsNotNull (uiController.CardKeyboard);
        }

    }
}
