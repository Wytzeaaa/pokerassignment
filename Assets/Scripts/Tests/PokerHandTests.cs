﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace PokerAssignment.Tests {

    public class PokerHandTests {
        

        [Test]
        public void ConstructorShouldNotBreakWhenGivingNullAsArgument () {
            var pokerHand = new PokerHand(null);
        }

        [Test]
        public void ShouldOrderPokerHandByScore () {
            var threeOfAKindHand1 = new PokerHand (
              new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R7, Card.SuitType.Diamond),
                    new Card (Card.RankType.R7, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R7, Card.SuitType.Club),
              });
            var invalidHand1 = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.Queen, Card.SuitType.Diamond),
                    new Card (Card.RankType.R7, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R7, Card.SuitType.Spade),
                });
            var invalidHand2 = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.Ace, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                });
            var twoPairHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R10, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                });
            var invalidHand3 = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R10, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                });

            var straightFlushHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R7, Card.SuitType.Spade),
                    new Card (Card.RankType.R5, Card.SuitType.Spade),
                    new Card (Card.RankType.R6, Card.SuitType.Spade),
                    new Card (Card.RankType.R8, Card.SuitType.Spade),
                });
            var threeOfAKindHand2 = new PokerHand (
               new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.King, Card.SuitType.Diamond),
                    new Card (Card.RankType.King, Card.SuitType.Heart),
                    new Card (Card.RankType.King, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
               });
            var highCardHand1 = new PokerHand (
               new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Heart),
                    new Card (Card.RankType.King, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
               });
            var highCardHand2 = new PokerHand (
               new Card[] {
                    new Card (Card.RankType.R3, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Heart),
                    new Card (Card.RankType.King, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
               });
            var orderedPokerHands = PokerHand.OrderPokerHandsByScore (new PokerHand[] {
                threeOfAKindHand1,
                invalidHand1,
                invalidHand2,
                twoPairHand,
                invalidHand3,
                straightFlushHand,
                threeOfAKindHand2,
                highCardHand1,
                highCardHand2
            });

            var expected = new PokerHand[] {
                straightFlushHand,
                threeOfAKindHand2,
                threeOfAKindHand1,
                twoPairHand,
                highCardHand2,
                highCardHand1,
                invalidHand1,
                invalidHand2,
                invalidHand3
            };

            CollectionAssert.AreEqual (expected, orderedPokerHands);
        }

        [Test]
        public void ShouldReturnBestPokerHandWhenGivenListOfPokerHands () {
            var straightHand1 = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.Ace, Card.SuitType.Club),
                    new Card (Card.RankType.R3, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R5, Card.SuitType.Club),
                });
            var straightHand2 = new PokerHand (
               new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R6, Card.SuitType.Club),
                    new Card (Card.RankType.R3, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R5, Card.SuitType.Club),
               });
            var hightCardHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.Ace, Card.SuitType.Diamond),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                    new Card (Card.RankType.King, Card.SuitType.Diamond),
                    new Card (Card.RankType.R7, Card.SuitType.Heart),
                    new Card (Card.RankType.R8, Card.SuitType.Diamond)
                });
            var bestPokerHand = PokerHand.GetBestPokerHand (
                new PokerHand[] {
                    straightHand1,
                    straightHand2,
                    hightCardHand
                } );
            Assert.AreEqual (straightHand2, bestPokerHand);
        }

        [Test]
        public void CategoryShouldReturnInvalidWhenCollectionSizeIsLessThan5 () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R3, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Club),
                    new Card (Card.RankType.R5, Card.SuitType.Club)
                });
            Assert.AreEqual (PokerHand.CategoryType.Invalid, pokerHand.Category);
        }

        [Test]
        public void GetCategoryShouldReturnInvalidWhenCollectionSizeIsMoreThan5 () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R3, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Club),
                    new Card (Card.RankType.R5, Card.SuitType.Club),
                    new Card (Card.RankType.R6, Card.SuitType.Club),
                    new Card (Card.RankType.R7, Card.SuitType.Club)
                });

            Assert.AreEqual (PokerHand.CategoryType.Invalid, pokerHand.Category);
        }

        [Test]
        public void GetCategoryShouldReturnInvalidWhenCollectionContainsDuplicatedCards () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R3, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Club),
                    new Card (Card.RankType.R5, Card.SuitType.Club),
                });

            Assert.AreEqual (PokerHand.CategoryType.Invalid, pokerHand.Category);
        }

        [Test]
        public void GetCategoryShouldReturnHighCardWhenCollectionIsHighCard () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Heart),
                    new Card (Card.RankType.R8, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                });

            Assert.AreEqual (PokerHand.CategoryType.HighCard, pokerHand.Category);
        }

        [Test]
        public void SortedHighCardHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Heart),
                    new Card (Card.RankType.R8, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club)
                });
            var expected = new Card[] {
                new Card (Card.RankType.R10, Card.SuitType.Club),
                new Card (Card.RankType.R8, Card.SuitType.Spade),
                new Card (Card.RankType.R6, Card.SuitType.Heart),
                new Card (Card.RankType.R4, Card.SuitType.Diamond),
                new Card (Card.RankType.R2, Card.SuitType.Club)
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnPairCardWhenCollectionIsPair () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                });

            Assert.AreEqual (PokerHand.CategoryType.Pair, pokerHand.Category);
        }

        [Test]
        public void SortedPairHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club)
                });
            var expected = new Card[] {
                new Card (Card.RankType.R4, Card.SuitType.Diamond),
                new Card (Card.RankType.R4, Card.SuitType.Spade),
                new Card (Card.RankType.R10, Card.SuitType.Club),
                new Card (Card.RankType.R6, Card.SuitType.Heart),
                new Card (Card.RankType.R2, Card.SuitType.Club)
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnTwoPairCardWhenCollectionIsTwoPair () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R10, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                });

            Assert.AreEqual (PokerHand.CategoryType.TwoPair, pokerHand.Category);
        }

        [Test]
        public void SortedTwoPairHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R10, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                });
            var expected = new Card[] {
                new Card (Card.RankType.R10, Card.SuitType.Heart),
                new Card (Card.RankType.R10, Card.SuitType.Club),
                new Card (Card.RankType.R4, Card.SuitType.Diamond),
                new Card (Card.RankType.R4, Card.SuitType.Spade),
                new Card (Card.RankType.R2, Card.SuitType.Club),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnThreeOfAKind () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                });

            Assert.AreEqual (PokerHand.CategoryType.ThreeOfAKind, pokerHand.Category);
        }

        [Test]
        public void SortedThreeOfAKindPairShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                });
            var expected = new Card[] {
                new Card (Card.RankType.R4, Card.SuitType.Diamond),
                new Card (Card.RankType.R4, Card.SuitType.Heart),
                new Card (Card.RankType.R4, Card.SuitType.Spade),
                new Card (Card.RankType.R10, Card.SuitType.Club),
                new Card (Card.RankType.R2, Card.SuitType.Club),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnStraight () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R6, Card.SuitType.Diamond),
                    new Card (Card.RankType.R7, Card.SuitType.Heart),
                    new Card (Card.RankType.R5, Card.SuitType.Spade),
                    new Card (Card.RankType.R8, Card.SuitType.Club),
                });

            Assert.AreEqual (PokerHand.CategoryType.Straigth, pokerHand.Category);
        }

        [Test]
        public void SortedStraightHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
               new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R6, Card.SuitType.Diamond),
                    new Card (Card.RankType.R7, Card.SuitType.Heart),
                    new Card (Card.RankType.R5, Card.SuitType.Spade),
                    new Card (Card.RankType.R8, Card.SuitType.Club),
               });
            var expected = new Card[] {
                new Card (Card.RankType.R8, Card.SuitType.Club),
                new Card (Card.RankType.R7, Card.SuitType.Heart),
                new Card (Card.RankType.R6, Card.SuitType.Diamond),
                new Card (Card.RankType.R5, Card.SuitType.Spade),
                new Card (Card.RankType.R4, Card.SuitType.Heart),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturFlush () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Diamond),
                    new Card (Card.RankType.Queen, Card.SuitType.Diamond),
                    new Card (Card.RankType.Ace, Card.SuitType.Diamond),
                    new Card (Card.RankType.R8, Card.SuitType.Diamond),
                });

            Assert.AreEqual (PokerHand.CategoryType.Flush, pokerHand.Category);
        }

        [Test]
        public void SortedFlushHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
               new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R6, Card.SuitType.Diamond),
                    new Card (Card.RankType.Queen, Card.SuitType.Diamond),
                    new Card (Card.RankType.Ace, Card.SuitType.Diamond),
                    new Card (Card.RankType.R8, Card.SuitType.Diamond),
               });
            var expected = new Card[] {
                new Card (Card.RankType.Ace, Card.SuitType.Diamond),
                new Card (Card.RankType.Queen, Card.SuitType.Diamond),
                new Card (Card.RankType.R8, Card.SuitType.Diamond),
                new Card (Card.RankType.R6, Card.SuitType.Diamond),
                new Card (Card.RankType.R4, Card.SuitType.Diamond),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnFullHouse () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R2, Card.SuitType.Spade),
                });

            Assert.AreEqual (PokerHand.CategoryType.FullHouse, pokerHand.Category);
        }

        [Test]
        public void SortedFullHouseHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R2, Card.SuitType.Spade),
                });
            var expected = new Card[] {
                new Card (Card.RankType.R4, Card.SuitType.Diamond),
                new Card (Card.RankType.R4, Card.SuitType.Heart),
                new Card (Card.RankType.R4, Card.SuitType.Spade),
                new Card (Card.RankType.R2, Card.SuitType.Club),
                new Card (Card.RankType.R2, Card.SuitType.Spade),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnFourOfAKind () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R2, Card.SuitType.Spade),
                });

            Assert.AreEqual (PokerHand.CategoryType.FourOfAKind, pokerHand.Category);
        }

        [Test]
        public void SortedFourOfAKindHandShoudBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R2, Card.SuitType.Spade),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                });
            var expected = new Card[] {
                new Card (Card.RankType.R4, Card.SuitType.Club),
                new Card (Card.RankType.R4, Card.SuitType.Heart),
                new Card (Card.RankType.R4, Card.SuitType.Spade),
                new Card (Card.RankType.R4, Card.SuitType.Diamond),
                new Card (Card.RankType.R2, Card.SuitType.Spade),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnStraightFlush () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R7, Card.SuitType.Spade),
                    new Card (Card.RankType.R5, Card.SuitType.Spade),
                    new Card (Card.RankType.R6, Card.SuitType.Spade),
                    new Card (Card.RankType.R8, Card.SuitType.Spade),
                });

            Assert.AreEqual (PokerHand.CategoryType.StraigthFlush, pokerHand.Category);
        }

        [Test]
        public void SortedStraightFlushHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R7, Card.SuitType.Spade),
                    new Card (Card.RankType.R5, Card.SuitType.Spade),
                    new Card (Card.RankType.R6, Card.SuitType.Spade),
                    new Card (Card.RankType.R8, Card.SuitType.Spade),
                });
            var expected = new Card[] {
                new Card (Card.RankType.R8, Card.SuitType.Spade),
                new Card (Card.RankType.R7, Card.SuitType.Spade),
                new Card (Card.RankType.R6, Card.SuitType.Spade),
                new Card (Card.RankType.R5, Card.SuitType.Spade),
                new Card (Card.RankType.R4, Card.SuitType.Spade),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void SortedWheelStraightFlushHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R3, Card.SuitType.Spade),
                    new Card (Card.RankType.R2, Card.SuitType.Spade),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.Ace, Card.SuitType.Spade),
                    new Card (Card.RankType.R5, Card.SuitType.Spade),
                });
            var expected = new Card[] {
                new Card (Card.RankType.R5, Card.SuitType.Spade),
                new Card (Card.RankType.R4, Card.SuitType.Spade),
                new Card (Card.RankType.R3, Card.SuitType.Spade),
                new Card (Card.RankType.R2, Card.SuitType.Spade),
                new Card (Card.RankType.Ace, Card.SuitType.Spade),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void GetCategoryShouldReturnRoyalFlush () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.King, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Spade),
                    new Card (Card.RankType.Ace, Card.SuitType.Spade),
                    new Card (Card.RankType.Queen, Card.SuitType.Spade),
                    new Card (Card.RankType.Jack, Card.SuitType.Spade),
                });

            Assert.AreEqual (PokerHand.CategoryType.RoyalFlush, pokerHand.Category);
        }

        [Test]
        public void SortedRoyalFlushHandShouldBeSortedCorrectly () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.King, Card.SuitType.Spade),
                    new Card (Card.RankType.R10, Card.SuitType.Spade),
                    new Card (Card.RankType.Ace, Card.SuitType.Spade),
                    new Card (Card.RankType.Queen, Card.SuitType.Spade),
                    new Card (Card.RankType.Jack, Card.SuitType.Spade),
                });
            var expected = new Card[] {
                new Card (Card.RankType.Ace, Card.SuitType.Spade),
                new Card (Card.RankType.King, Card.SuitType.Spade),
                new Card (Card.RankType.Queen, Card.SuitType.Spade),
                new Card (Card.RankType.Jack, Card.SuitType.Spade),
                new Card (Card.RankType.R10, Card.SuitType.Spade),
            };
            CollectionAssert.AreEqual (expected, pokerHand.SortedCards);
        }

        [Test]
        public void IsStraight_ShouldReturnTrue_WhenCardCollectionIsWheel () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R5, Card.SuitType.Diamond),
                    new Card (Card.RankType.R2, Card.SuitType.Diamond),
                    new Card (Card.RankType.Ace, Card.SuitType.Diamond),
                    new Card (Card.RankType.R4, Card.SuitType.Diamond),
                    new Card (Card.RankType.R3, Card.SuitType.Diamond)
                });
        }

        [Test]
        public void GetCategoryShouldReturnStraightWhenGivenAC2345 () {
            var pokerHand = new PokerHand (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Club),
                    new Card (Card.RankType.Ace, Card.SuitType.Club),
                    new Card (Card.RankType.R3, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R5, Card.SuitType.Club),
                });
            Assert.AreEqual (PokerHand.CategoryType.Straigth, pokerHand.Category);
        }





    }
}
