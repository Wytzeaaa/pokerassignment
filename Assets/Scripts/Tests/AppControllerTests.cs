﻿using NUnit.Framework;
using System.Collections;
using System.Linq;
using UnityEditor.Build.Content;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace PokerAssignment.Tests {

    public class AppControllerTests {

        public const string MainSceneName = "SampleScene";

        public static AppController GetAppControllerInstance () {
            var mainScene = SceneManager.GetSceneByName (MainSceneName);
            var rootGameObjects = mainScene.GetRootGameObjects ();
            foreach (var rootGameObject in rootGameObjects) {
                var appController = rootGameObject.GetComponentInChildren<AppController> ();
                if (appController != null) {
                    return appController;
                }
            }
            Assert.Fail ("AppController instance could not be found.");
            return null;
        }

        [Test]
        public void SceneMainShouldBeIncludedAndActiveInBuildSettings () {
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++) {
                if (string.Equals (SceneManager.GetSceneByBuildIndex (i).name, MainSceneName))
                    Assert.Pass ();
            };
            Assert.Fail ($"Expected: {MainSceneName}");
        }

        [Test]
        public void SceneMainShouldContainAppControllerInstance() {
            var mainScene = SceneManager.GetSceneByName (MainSceneName);
            int numAppControllers = 0;
            var rootGameObjects = mainScene.GetRootGameObjects ();
            foreach (var rootGameObject in rootGameObjects) {
                numAppControllers += rootGameObject.GetComponentsInChildren<AppController> ().Length;
            }
            Assert.AreEqual (1, numAppControllers, $"{numAppControllers} AppController instance(s) found in scene.");
        }

        [Test]
        public void UiControllerShouldBeAssigned() {
            var appController = GetAppControllerInstance ();
            Assert.IsNotNull (appController.UiController);
        }

    }
}
