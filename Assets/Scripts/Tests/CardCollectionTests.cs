﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokerAssignment.Tests {

    public class CardCollectionTests {

        [Test]
        public void CollectionsCanBeAdded () {
            var c1 = new CardCollection (
                new Card[] {
                    new Card (Card.RankType.Queen, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Heart)
                });
            var c2 = new CardCollection (
               new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Spade)
               });
            var c3 = new CardCollection (
               new Card[] {
                    new Card (Card.RankType.Ace, Card.SuitType.Heart)
               });

            var actual = c2 + c3 +c1;
            var expected = new CardCollection (
                new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Spade),
                    new Card (Card.RankType.Ace, Card.SuitType.Heart),
                    new Card (Card.RankType.Queen, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Heart)
                });
            CollectionAssert.AreEqual (expected, actual);
        }

        [Test]
        public void CollectionsCanSubtracted () {
            var c1 = new CardCollection (
                new Card[] {
                   new Card (Card.RankType.R2, Card.SuitType.Spade),
                    new Card (Card.RankType.Ace, Card.SuitType.Heart),
                    new Card (Card.RankType.Queen, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Heart)
                });
            var c2 = new CardCollection (
               new Card[] {
                    new Card (Card.RankType.R2, Card.SuitType.Spade),
                   new Card (Card.RankType.Queen, Card.SuitType.Heart),
               });

            var actual = c1 - c2;
            var expected = new CardCollection (
                new Card[] {
                    new Card (Card.RankType.Ace, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Heart)
                });
            CollectionAssert.AreEqual (expected, actual);
        }

        [Test]
        public void CollectionShouldBeSorteddByRankDescending () {
            var actual = new CardCollection (
                new Card[] {
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                    new Card (Card.RankType.Queen, Card.SuitType.Heart),
                    new Card (Card.RankType.R2, Card.SuitType.Diamond),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.King, Card.SuitType.Club),
                    new Card (Card.RankType.Queen, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R3, Card.SuitType.Heart),
                    new Card (Card.RankType.Queen, Card.SuitType.Diamond),
                    new Card (Card.RankType.R5, Card.SuitType.Club)
                });

            var expected = new CardCollection (
                new Card[] {
                    new Card (Card.RankType.King, Card.SuitType.Club),
                    new Card (Card.RankType.Queen, Card.SuitType.Heart),
                    new Card (Card.RankType.Queen, Card.SuitType.Club),
                    new Card (Card.RankType.Queen, Card.SuitType.Diamond),
                    new Card (Card.RankType.R10, Card.SuitType.Club),
                    new Card (Card.RankType.R5, Card.SuitType.Club),
                    new Card (Card.RankType.R4, Card.SuitType.Heart),
                    new Card (Card.RankType.R4, Card.SuitType.Spade),
                    new Card (Card.RankType.R3, Card.SuitType.Heart),
                    new Card (Card.RankType.R2, Card.SuitType.Diamond)
                });

            CollectionAssert.AreEqual (expected, actual.SortedByRank);
        }

        [Test]
        public void CollectionShouldReturnNthElement () {
            var collection = new CardCollection (
                new Card[] {
                        new Card (Card.RankType.R8, Card.SuitType.Heart),
                        new Card (Card.RankType.Ace, Card.SuitType.Club),
                        new Card (Card.RankType.R5, Card.SuitType.Diamond),
                });

            Assert.AreEqual (new Card (Card.RankType.Ace, Card.SuitType.Club), collection[1]);
        }

        [Test]
        public void CollectionShouldReturnLength () {
            var collection = new CardCollection (
                new Card[] {
                    new Card (Card.RankType.King, Card.SuitType.Club),
                    new Card (Card.RankType.Queen, Card.SuitType.Heart),
                    new Card (Card.RankType.Queen, Card.SuitType.Club),
                    new Card (Card.RankType.Queen, Card.SuitType.Diamond)
            });

            Assert.AreEqual (4, collection.Count);
        }

        [Test]
        public void ContainsDuplicatesShouldReturnTrueWhenCollectionContainsDuplicates () {
            var collection = new CardCollection (
                new Card[] {
                    new Card (Card.RankType.Ace, Card.SuitType.Heart),
                    new Card (Card.RankType.Jack, Card.SuitType.Heart),
                    new Card (Card.RankType.Ace, Card.SuitType.Heart),
                    new Card (Card.RankType.Jack, Card.SuitType.Spade)
            });
            Assert.IsTrue (collection.ContainsDuplicates);
        }
    }
}