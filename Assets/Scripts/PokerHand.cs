﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PokerAssignment {

    public class PokerHand {

        public const int HandSize = 5;

        public CardCollection UnsortedCards { get; private set; }
        public CardCollection SortedCards => GetSortedCardCollection ();
        public CategoryType Category => GetCategory ();
        public bool IsStraight => GetIsStraight ();
        public bool IsFlush => GetIsFlush ();

        public PokerHand (IEnumerable<Card> unsortedCards) {
            UnsortedCards = new CardCollection (unsortedCards);
        }

        public override string ToString () {
            return $"{base.ToString ()} ({UnsortedCards.ToString()})";
        }

        public static IReadOnlyList<PokerHand> GetPossiblePokerHands (CardCollection cardCollection) {
            //https://stackoverflow.com/questions/33336540/how-to-use-linq-to-find-all-combinations-of-n-items-from-a-set-of-numbers
            var pokerHands = new List<PokerHand> ();
            if (cardCollection.Count <= HandSize) {
                return new PokerHand[0];
            }

            int[] cardIndexes = Enumerable.Range (0, HandSize).ToArray ();

            do {
                pokerHands.Add (new PokerHand (cardIndexes.Select (i => cardCollection[i])));
                SetIndexes (cardIndexes, cardIndexes.Length - 1, cardCollection.Count);
            }
            while (!AllPlacesChecked (cardIndexes, cardCollection.Count));

            void SetIndexes (int[] indexes, int lastIndex, int count) {
                indexes[lastIndex]++;
                if (lastIndex > 0 && indexes[lastIndex] == count) {
                    SetIndexes (indexes, lastIndex - 1, count - 1);
                    indexes[lastIndex] = indexes[lastIndex - 1] + 1;
                }
            }

            bool AllPlacesChecked (int[] indexes, int places) {
                for (int i = indexes.Length - 1; i >= 0; i--) {
                    if (indexes[i] != places)
                        return false;
                    places--;
                }
                return true;
            }

            return pokerHands;
        }

        

        public static PokerHand GetBestPokerHand (IEnumerable<PokerHand> pokerHands) {
            var orderedPokerhands = OrderPokerHandsByScore (pokerHands);
            return (orderedPokerhands.Count > 0) ? orderedPokerhands[0] : null;
        }

        public static IReadOnlyList<PokerHand> OrderPokerHandsByScore (IEnumerable<PokerHand> unorderedPokerHands) {
            var orderedPokerHands = unorderedPokerHands
                .Where (e => e.Category != CategoryType.Invalid)
                .OrderByDescending (e => (int)e.Category)
                .ThenByDescending (e => e.SortedCards[0].Rank)
                .ThenByDescending (e => e.SortedCards[1].Rank)
                .ThenByDescending (e => e.SortedCards[2].Rank)
                .ThenByDescending (e => e.SortedCards[3].Rank)
                .ThenByDescending (e => e.SortedCards[4].Rank)
                .Concat (unorderedPokerHands.Where (e => e.Category == CategoryType.Invalid));

            return orderedPokerHands.ToList<PokerHand>();
        }
        
        public static int[] GePokerHandsScoreIndexes (IEnumerable<PokerHand> unorderedPokerHands) {
            var unordered = unorderedPokerHands.ToList ();
            int[] scores = new int[unordered.Count];
            var ordered = OrderPokerHandsByScore (unorderedPokerHands).ToList();
            for (int i = 0; i < ordered.Count; i++) {
                var index = unordered.IndexOf (ordered[i]);
                scores[index] = i;
            }
            return scores;
        }

        private CategoryType GetCategory () {
            if (UnsortedCards.Count != HandSize) {
                return CategoryType.Invalid;
            }
            if (UnsortedCards.ContainsDuplicates) {
                return CategoryType.Invalid;
            }
            if (IsStraight) {
                if (IsFlush) {
                    if (UnsortedCards.SortedByRank[0].Rank == Card.RankType.Ace && UnsortedCards.SortedByRank[1].Rank == Card.RankType.King) {
                        return CategoryType.RoyalFlush;
                    } else {
                        return CategoryType.StraigthFlush;
                    }
                } else {
                    return CategoryType.Straigth;
                }
            } else {
                if (IsFlush) {
                    return CategoryType.Flush;
                } else if (GetUnsortedCardsGroupedByRank ()[0].Count == 4) {
                    return CategoryType.FourOfAKind;
                } else if (GetUnsortedCardsGroupedByRank ()[0].Count == 3) {
                    if (GetUnsortedCardsGroupedByRank ()[1].Count == 2) {
                        return CategoryType.FullHouse;
                    } else {
                        return CategoryType.ThreeOfAKind;
                    }
                } else if (GetUnsortedCardsGroupedByRank ()[0].Count == 2) {
                    if (GetUnsortedCardsGroupedByRank ()[1].Count == 2) {
                        return CategoryType.TwoPair;
                    } else {
                        return CategoryType.Pair;
                    }
                }
            }
            return CategoryType.HighCard;
        }

        private CardCollection GetSortedCardCollection () {
            var list = new List<string> ();
            var category = GetCategory ();
            if (category == CategoryType.Invalid) {
                return new CardCollection (UnsortedCards);
            } else if (category == CategoryType.HighCard) {
                return new CardCollection(UnsortedCards.SortedByRank);
            } else if (category == CategoryType.Pair || category == CategoryType.ThreeOfAKind || category == CategoryType.FourOfAKind) {
                return GetUnsortedCardsGroupedByRank ()[0] + (UnsortedCards - GetUnsortedCardsGroupedByRank ()[0]).SortedByRank;
            } else if (category == CategoryType.TwoPair || category == CategoryType.FullHouse) {
                return GetUnsortedCardsGroupedByRank ()[0] + GetUnsortedCardsGroupedByRank ()[1] + (UnsortedCards - GetUnsortedCardsGroupedByRank ()[0] - GetUnsortedCardsGroupedByRank ()[1]).SortedByRank;
            } else if (category == CategoryType.Flush || category == CategoryType.RoyalFlush) {
                return new CardCollection(UnsortedCards.SortedByRank);
            } else if (category == CategoryType.Straigth || category == CategoryType.StraigthFlush) {
                if (GetIsWheel ()) {
                    return (UnsortedCards.SortedByRank - UnsortedCards.SortedByRank[0]) + UnsortedCards.SortedByRank[0];
                } else {
                    return new CardCollection (UnsortedCards.SortedByRank);
                }
            }
            return new CardCollection (UnsortedCards);
        }

        private bool GetIsStraight () {
            var sortedCards = UnsortedCards.SortedByRank;

            //Start counting at the second card when there is the possibility that this is a straight that uses Ace as the lowest card (a 'wheel').
            int startIndex = (GetIsWheel()) ? 1 : 0;
            
            var rank = sortedCards[startIndex].Rank;
            
            for (int i = startIndex; i < sortedCards.Count; i++) {
                if (sortedCards[i].Rank != rank) {
                    return false;
                }
                rank--;
            }
            return true;
        }

        private bool GetIsWheel () {
            return UnsortedCards.SortedByRank[0].Rank == Card.RankType.Ace && UnsortedCards.SortedByRank[UnsortedCards.SortedByRank.Count - 1].Rank == Card.RankType.R2;
        }

        private bool GetIsFlush () {
            return UnsortedCards.GroupBy (c => c.Suit).Count () == 1;
        }

        private IReadOnlyList<CardCollection> GetUnsortedCardsGroupedByRank () {
            var r = UnsortedCards.SortedByRank.GroupBy (card => card.Rank).OrderByDescending (g => g.Count ()).Select (f => f.AsEnumerable ()).Select (f => new CardCollection (f));
            return r.ToList ().AsReadOnly ();
        }

        public enum CategoryType {
            Invalid,
            HighCard,
            Pair,
            TwoPair,
            ThreeOfAKind,
            Straigth,
            Flush,
            FullHouse,
            FourOfAKind,
            StraigthFlush,
            RoyalFlush
        }

    }

}
