﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace PokerAssignment {

    public class AppController : MonoBehaviour {

        public static AppController Instance { get; private set; }

        public Ui.UiController UiController => _uiController;

        [SerializeField] private Ui.UiController _uiController;

        private List<PokerHand> _pokerHands;

        private void Awake () {
            _pokerHands = new List<PokerHand> ();
            Instance = this;
        }
        
    }

}
