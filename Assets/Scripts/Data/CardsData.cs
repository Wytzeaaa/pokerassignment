﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PokerAssignment.Data {

    /// <summary>
    /// Holds all 52 cards in a deck.
    /// </summary>
    [CreateAssetMenu (fileName = "Cards", menuName = "ScriptableObjects/CardsData", order = 1)]
    public class CardsData : ScriptableObject {

        [System.Serializable]
        public class CardIcon {
            public Card Card;
            public Sprite Icon;
        }

        [SerializeField] private CardIcon[] _cardIcons;

        private Dictionary<Card, Sprite> _cardIconDict;

        private void Init () {
            _cardIconDict = new Dictionary<Card, Sprite> ();
            foreach (var cardIcon in _cardIcons) {
                _cardIconDict.Add (cardIcon.Card, cardIcon.Icon);
            }
        }

        public Sprite GetIcon (Card card) {
            Init ();
            return _cardIconDict[card];
        }

    }
}