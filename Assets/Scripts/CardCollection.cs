﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PokerAssignment {

    public class CardCollection : IReadOnlyList<Card> {

        private IReadOnlyList<Card> _cards;

        public Card this[int index] => _cards[index];
        public int Count => _cards.Count;
        public bool ContainsDuplicates => GetContainsDuplicates ();
        public CardCollection SortedByRank => GetSortedByRank ();

        public CardCollection (IEnumerable<Card> cards) {
            _cards = cards?.ToList();
        }

        public IEnumerator<Card> GetEnumerator () {
            return (_cards.GetEnumerator ()) as IEnumerator<Card>;
        }

        IEnumerator IEnumerable.GetEnumerator () {
            return _cards.GetEnumerator ();
        }

        public override string ToString () {
            var str = base.ToString () + " (";
            int maxEntries = 20;
            if (_cards != null) {
                for (int i = 0; i < Mathf.Min(maxEntries, _cards.Count); i++) {
                    str += $"{_cards[i].Suit} {_cards[i].Rank}";
                    if (i < _cards.Count - 1) {
                        str += ", ";
                    }
                    if (i == maxEntries - 1) {
                        str += "...";
                    }
                }
            } else {
                str += ")";
            }
            return str;
        }

        public static CardCollection operator + (CardCollection c1, CardCollection c2) {
            return new CardCollection (c1._cards.Concat (c2._cards));
        }

        public static CardCollection operator + (CardCollection collection, Card card) {
            return collection + new CardCollection (new Card[] { card });
        }

        public static CardCollection operator + (Card card, CardCollection collection) {
            return new CardCollection (new Card[] { card }) + collection;
        }

        public static CardCollection operator - (CardCollection c1, CardCollection c2) {
            var cardCollection = new CardCollection (c1);
            foreach (var cardToRemove in c2) {
                ((List<Card>)(cardCollection._cards)).Remove (cardToRemove);
            }
            return cardCollection;
        }

        public static CardCollection operator - (CardCollection collection, Card card) {
            return collection - new CardCollection (new Card[] { card });
        }

        public static CardCollection operator - (Card card, CardCollection collection) {
            return new CardCollection (new Card[] { card }) - collection;
        }

        private CardCollection GetSortedByRank() {
            return new CardCollection (
                _cards.OrderByDescending(e=>e.Rank)
            );
        }

        private bool GetContainsDuplicates () {
            return _cards.GroupBy (e => new { e.Suit, e.Rank }).Any (g => g.Count () > 1);
        }

    }

}
