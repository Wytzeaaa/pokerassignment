﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokerAssignment.Ui {

    public class CardKeyboard : MonoBehaviour {

        public IReadOnlyList<CardButton> CardButtons => _cardButtons;
        public KeyboardButton BackspaceButton => _backSpaceButton;
        public KeyboardButton EnterButton => _enterButton;

        [SerializeField] private CardButton[] _cardButtons;
        [SerializeField] private KeyboardButton _backSpaceButton;
        [SerializeField] private KeyboardButton _enterButton;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private Vector2 _shownPosition, _hiddenPosition;

        private void Awake () {
            if (_cardButtons == null) {
                _cardButtons = GetComponentsInChildren<CardButton> ();
            }
            AddCardButtonListeners ();
            
        }

        public delegate void CardClickedHandler (Card card);
        public delegate void ClickedHandler ();


        public event CardClickedHandler CardClicked;
        public event ClickedHandler BackspaceClicked;
        public event ClickedHandler EnterClicked;


        private void AddCardButtonListeners () {
            foreach (var cardButton in _cardButtons) {
                cardButton.Clicked += HandleCardButtonClicked;
            }
            _backSpaceButton.Clicked += HandleBackspaceButtonClicked;
            _enterButton.Clicked += handleEnterButtonClicked;
        }

        private void handleEnterButtonClicked () {
            EnterClicked?.Invoke ();
        }

        private void HandleBackspaceButtonClicked () {
            BackspaceClicked?.Invoke ();
        }

        private void HandleCardButtonClicked (Card card) {
            CardClicked?.Invoke (card);
        }

        public void Show() {
            StartCoroutine (TweenToPosition (_shownPosition));
        }

        public void Hide () {
            StartCoroutine (TweenToPosition (_hiddenPosition));
        }

        private IEnumerator TweenToPosition (Vector2 endPosition) {
            Vector2 startPosition = _rectTransform.anchoredPosition;
            float i = 0;
            float duration = .15f;
            while (i < 1f) {
                _rectTransform.anchoredPosition = Vector2.Lerp (startPosition, endPosition, -1 * i * (i - 2)); //quad ease out.
                i += Time.deltaTime / duration;
                yield return null;
            }

            _rectTransform.anchoredPosition = endPosition;
            yield return null;
        }

        
    }

}
