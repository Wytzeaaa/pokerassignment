﻿using PokerAssignment.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PokerAssignment.Ui {

    public class CardInputField : SelectableElement {

        private int _caretIndex;
        private List<Card> _cards;

        [SerializeField] private Image _caret;
        [SerializeField] private Image[] _cardSymbols;
        [SerializeField] private Image[] _cardIcons;
        [SerializeField] private float[] _caretPositions;
        [SerializeField] private CardsData _cardsData;
        [SerializeField] private Image _scoreButton;
        [SerializeField] private Text _scoreText;

        public PokerHand PokerHand { get; private set; }

        private void Awake () {
            _cards = new List<Card> ();
            PokerHand = new PokerHand (_cards);
            _caret.enabled = false;
        }

        public override void OnPointerClick (PointerEventData eventData) {
            _caretIndex = PointerPositionToCaretIndex (eventData.pressPosition);
            UpdateCaretPosition ();
            base.OnPointerClick (eventData);
        }

        public override void OnSelect () {
            AppController.Instance.UiController.CardKeyboard.CardClicked += HandleKeyboardCardClicked;
            AppController.Instance.UiController.CardKeyboard.BackspaceClicked += HandleBackspaceClicked;
            _caret.enabled = true;
            base.OnSelect ();
        }

        public override void OnDeselect () {
            AppController.Instance.UiController.CardKeyboard.CardClicked -= HandleKeyboardCardClicked;
            AppController.Instance.UiController.CardKeyboard.BackspaceClicked -= HandleBackspaceClicked;
            _caret.enabled = false;
            base.OnDeselect ();
        }

        public void SetScoreIndex (int scoreIndex) {
            _scoreText.text = scoreIndex.ToString();
        }

        private void HandleKeyboardCardClicked (Card card) {
            AddCard (card);
        }

        private void HandleBackspaceClicked () {
            RemoveCard ();
        }

        private void AddCard (Card card) {
            if (_cards.Count < _cardSymbols.Length) {
                _cards.Insert (_caretIndex, card);
                _caretIndex++;
                UpdateCaretPosition ();
                UpdateCardIcons ();
                UpdatePokerHand ();
            }
        }

        private void RemoveCard() {
            if (_caretIndex > 0) {
                _cards.RemoveAt (_caretIndex - 1);
                _caretIndex--;
                UpdateCaretPosition ();
                UpdateCardIcons ();
                UpdatePokerHand ();
            }
        }
        
        private int PointerPositionToCaretIndex (Vector2 pointerPosition) {
            Vector2 localPos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle (this.GetComponent<RectTransform> (), pointerPosition, null, out localPos);
            float minDelta = Mathf.Infinity;
            int caretIndex = 0;

            //find closest caretPosition;
            for (int i = 0; i < _caretPositions.Length; i++) {
                var delta = Mathf.Abs (_caretPositions[i] - localPos.x);
                if (delta < minDelta) {
                    minDelta = delta;
                    caretIndex = i;
                }
            }

            //Move caretindex to the left if possible.
            while (caretIndex > 0 && caretIndex > _cards.Count ) {
                caretIndex--;
            }
            return caretIndex;
        }

        private void UpdateCaretPosition () {
            _caret.rectTransform.anchoredPosition = new Vector2 (_caretPositions[_caretIndex], _caret.rectTransform.anchoredPosition.y);
        }

        private void UpdateCardIcons () {
            for (int i = 0; i < _cardSymbols.Length; i++) {
                bool show = i < _cards.Count;
                _cardSymbols[i].gameObject.SetActive (show);
                if (show) {
                    _cardIcons[i].sprite = _cardsData.GetIcon (_cards[i]);
                }
            }
            PokerHand = new PokerHand (_cards);
        }

        private void UpdatePokerHand() {
            PokerHand = new PokerHand (_cards);
            CardsUpdated?.Invoke (this);
        }

        public delegate void CardsUpdatedHandler (CardInputField inputField);
        public event CardsUpdatedHandler CardsUpdated;

    }
}
