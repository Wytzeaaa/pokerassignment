﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PokerAssignment.Ui {

    public class UiController : MonoBehaviour {

        public CardKeyboard CardKeyboard => _cardKeyboard;

        public Vector2 _firstInputFieldPosition;
        public Vector2 _inputFieldsDistance;

        public struct CardIcon {
            public Card Card;
            public Texture2D Icon;
        }

        [SerializeField] private CardKeyboard _cardKeyboard;
        [SerializeField] private CardInputField _cardInputFieldPrefab;
        [SerializeField] private RectTransform _table;
        [SerializeField] private ScrollRect _scrollRect;

        private List<CardInputField> _inputFields;

        private void Awake () {
            _inputFields = new List<CardInputField> ();
            for (int i = 0; i < 10; i++) {
                AddInputField ();
            }
            SelectableElement.SelectedChanged += HandleSelectedElementChanged;
        }

        private void HandleSelectedElementChanged (SelectableElement element) {
            if (element == null || !(element is CardInputField)) {
                _cardKeyboard.Hide ();
            } else {
                _cardKeyboard.Show ();
            }
        }

        public void AddInputField () {
            var cardInputField = GameObject.Instantiate<CardInputField> (_cardInputFieldPrefab, _table);
            _inputFields.Add (cardInputField);
            cardInputField.CardsUpdated += HandleCardInputFieldUpdated;
            PositionInputFields ();
        }

        private void HandleCardInputFieldUpdated (CardInputField inputField) {
            UpdateScoreIndex ();
        }

        private void UpdateScoreIndex() {
            List<PokerHand> pokerHands = new List<PokerHand> ();
            for (int i = 0; i < _inputFields.Count; i++) {
                pokerHands.Add (_inputFields[i].PokerHand);
            }
            int[] scoreIndexes = PokerHand.GePokerHandsScoreIndexes (pokerHands);
            for (int i = 0; i < _inputFields.Count; i++) {
                _inputFields[i].SetScoreIndex (scoreIndexes[i]);
            }
        }

        public void PositionInputFields () {
            for (int i = 0; i < _inputFields.Count; i++) {
                _inputFields[i].GetComponent<RectTransform> ().anchoredPosition = _firstInputFieldPosition + i * _inputFieldsDistance;
            }
            _table.sizeDelta = new Vector2 (0, -((_inputFields.Count - 1)* _inputFieldsDistance.y + 2f * _firstInputFieldPosition.y));
        }

    }

}
