﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PokerAssignment.Ui {

    public class KeyboardButton : MonoBehaviour, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler {

        public delegate void ClickedHandler ();

        public event ClickedHandler Clicked;

        public virtual void OnPointerClick (PointerEventData eventData) {
            Clicked?.Invoke ();
        }

        public void OnPointerUp (PointerEventData eventData) {
            //throw new System.NotImplementedException ();
        }

        public void OnPointerDown (PointerEventData eventData) {
            //throw new System.NotImplementedException ();
        }
        
    }
}
