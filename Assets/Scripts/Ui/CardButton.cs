﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace PokerAssignment.Ui {

    public class CardButton : KeyboardButton {

        public Card Card => _card;

        [SerializeField] private Card _card;

        new public delegate void ClickedHandler (Card card);

        new public event ClickedHandler Clicked;

        public override void OnPointerClick (PointerEventData eventData) {
            Clicked?.Invoke (_card);
        }

    }

}
