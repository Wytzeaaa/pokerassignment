﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PokerAssignment.Ui {

    public class SelectableElement : MonoBehaviour, IPointerClickHandler {

        public static SelectableElement Current { get; private set; }

        private static List<SelectableElement> _selectableElements = new List<SelectableElement> ();

        private bool _isSelected = false;
        private RectTransform _rectTransform;

        public RectTransform rectTransform {
            get {
                if (_rectTransform == null) {
                    _rectTransform = GetComponent<RectTransform> ();
                }
                return _rectTransform;
            }
        }

        /// <summary>
        /// Set the selected object manually. Use 'null' to select nothing.
        /// </summary>
        /// <param name="element"></param>
        public static void Select(SelectableElement element) {
            HandleClicked (element);
        }

        private static void AddElement(SelectableElement element) {
            _selectableElements.Add (element);
            element.Clicked += HandleClicked;
        }

        private static void RemoveElement(SelectableElement element) {
            _selectableElements.Remove (element);
            element.Clicked -= HandleClicked;
        }

        private static void HandleClicked(SelectableElement element) {
            Current = element;
            //Two loops to make sure OnDeselect is always called before OnSelect.
            for (int i = _selectableElements.Count - 1; i>=0; i--) {
                var selectableElement = _selectableElements[i];
                if (selectableElement != element && selectableElement._isSelected) {
                    selectableElement._isSelected = false;
                    selectableElement.OnDeselect ();
                }
            }
            for (int i = _selectableElements.Count - 1; i >= 0; i--) {
                var selectableElement = _selectableElements[i];
                if (selectableElement == element && !selectableElement._isSelected) {
                    selectableElement._isSelected = true;
                    selectableElement.OnSelect ();
                    SelectedChanged?.Invoke (element);
                } 
            }
            if (element == null) {
                Debug.Log ("S+" + element);
                SelectedChanged?.Invoke (null);
            }

        }

        private void OnEnable () {
            AddElement (this);
        }

        private void OnDisable () {
            RemoveElement (this);
        }

        public virtual void OnPointerClick (PointerEventData eventData) {
            Clicked?.Invoke (this);
        }

        public virtual void OnSelect() {
        }

        public virtual void OnDeselect() {
        }

        public delegate void SelectedChangedHandler (SelectableElement element);
        public static event SelectedChangedHandler SelectedChanged;

        public delegate void ClickedHandler (SelectableElement element);
        public event ClickedHandler Clicked;

    }
}
