﻿using PokerAssignment.Data;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace PokerAssignment.Ui {

    [CustomEditor (typeof (CardKeyboard))]
    public class CardKeyboardEditor : Editor {

        public override void OnInspectorGUI () {
            if (GUILayout.Button ("Set up card buttons...")) {
                SetUpCardButtons ();
            }
            base.OnInspectorGUI ();
        }

        private void SetUpCardButtons () {

            var numSuits = Enum.GetValues (typeof (Card.SuitType)).Length;
            var numRanks = Enum.GetValues (typeof (Card.RankType)).Length;
            int index = 0;
            for (int suitIndex = 0; suitIndex < numSuits; suitIndex++) {
                for (int rankIndex = 0; rankIndex < numRanks; rankIndex++) {
                    var cardButtonObject = new SerializedObject (((CardKeyboard)target).CardButtons[index]);
                    cardButtonObject.FindProperty ("_card").FindPropertyRelative ("_suit").enumValueIndex = suitIndex;
                    cardButtonObject.FindProperty ("_card").FindPropertyRelative ("_rank").enumValueIndex = rankIndex;
                    cardButtonObject.ApplyModifiedPropertiesWithoutUndo ();
                    var face = ((CardKeyboard)target).CardButtons[index].transform.Find ("Face").GetComponent<Image> ();
                    var faceSpritePath = $"Assets/Textures/UI/CardFaces/card_suit{suitIndex}_value{rankIndex.ToString ("00")}.psd";
                    var faceSprite = AssetDatabase.LoadAssetAtPath<Sprite> (faceSpritePath);
                    face.sprite = faceSprite;
                    index++;
                }
            }

        }

    }
}
