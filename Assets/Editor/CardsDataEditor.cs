﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PokerAssignment.Data {

    [CustomEditor (typeof (CardsData))]
    public class CardsDataEditor : Editor {

        public override void OnInspectorGUI () {
            if (GUILayout.Button ("Auto fill...")) {
                AutoFill ();
            }
            base.OnInspectorGUI ();
        }

        private void AutoFill () {
            var cardsData = (CardsData)target;
            SerializedProperty cardIconsProperty = serializedObject.FindProperty ("_cardIcons");
            cardIconsProperty.arraySize = 0;
            cardIconsProperty.serializedObject.ApplyModifiedPropertiesWithoutUndo ();
            var numSuits = Enum.GetValues (typeof (Card.SuitType)).Length;
            var numRanks = Enum.GetValues (typeof (Card.RankType)).Length;
            for (int suitIndex = 0; suitIndex < numSuits; suitIndex++) {
                for (int rankIndex = 0; rankIndex < numRanks; rankIndex++) {
                    cardIconsProperty.arraySize++;
                    cardIconsProperty.serializedObject.ApplyModifiedPropertiesWithoutUndo ();
                    var cardIconsElementProperty = cardIconsProperty.GetArrayElementAtIndex (cardIconsProperty.arraySize - 1);
                    cardIconsElementProperty.serializedObject.ApplyModifiedProperties ();
                    cardIconsElementProperty.FindPropertyRelative ("Card").FindPropertyRelative ("_suit").enumValueIndex = suitIndex;
                    cardIconsElementProperty.FindPropertyRelative ("Card").FindPropertyRelative ("_rank").enumValueIndex = rankIndex;
                    var faceSpritePath = $"Assets/Textures/UI/CardFaces/card_suit{suitIndex}_value{rankIndex.ToString ("00")}.psd";
                    var faceSprite = AssetDatabase.LoadAssetAtPath<Sprite> (faceSpritePath);
                    cardIconsElementProperty.FindPropertyRelative ("Icon").objectReferenceValue = faceSprite;

                }
            }
            cardIconsProperty.serializedObject.ApplyModifiedProperties ();

        }

    }
}
